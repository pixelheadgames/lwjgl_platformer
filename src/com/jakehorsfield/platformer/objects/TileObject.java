package com.jakehorsfield.platformer.objects;

import com.jakehorsfield.platformer.objects.tiles.TileType;
import org.newdawn.slick.opengl.Texture;

import java.awt.*;

public abstract class TileObject extends GameObject {

    protected Texture texture;
    protected TileType type;

    public TileType getType() {
        return type;
    }

    public void setType(TileType type) {
        this.type = type;
    }

    public Rectangle getBounds() {
        return new Rectangle((int) position.x * 32, (int) position.y * 32, (int) width, (int) height);
    }
}
