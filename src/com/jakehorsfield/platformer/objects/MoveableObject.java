package com.jakehorsfield.platformer.objects;

import org.lwjgl.util.vector.Vector2f;

public abstract class MoveableObject extends GameObject {

    protected Vector2f movement = new Vector2f();

    public abstract void update(double delta);

    public void setMovementX(float mx) {
        movement.x = mx;
    }

    public void setMovementY(float my) {
        movement.y = my;
    }

    public float getMovementX() {
        return movement.x;
    }

    public float getMovementY() {
        return movement.y;
    }
}
