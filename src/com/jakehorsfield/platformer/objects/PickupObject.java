package com.jakehorsfield.platformer.objects;

import com.jakehorsfield.platformer.player.Player;

public abstract class PickupObject extends GameObject {

    protected Player player;

    public abstract void onPickup();

    public abstract void update();

    public abstract void render();
}
