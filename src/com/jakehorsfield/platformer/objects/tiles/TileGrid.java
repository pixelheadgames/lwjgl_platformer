package com.jakehorsfield.platformer.objects.tiles;

import com.jakehorsfield.platformer.objects.TileObject;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
import org.lwjgl.opengl.Display;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TileGrid {

    public static final int AMOUNT_TILES_WIDTH = Display.getWidth() / 32;
    public static final int AMOUNT_TILES_HEIGHT = Display.getHeight() / 32 + 1;
    public static TileObject[][] tiles = new TileObject[AMOUNT_TILES_WIDTH][AMOUNT_TILES_HEIGHT];

    public TileGrid() {
        for (int x = 0; x < AMOUNT_TILES_WIDTH; x++) {
            for (int y = 0; y < AMOUNT_TILES_HEIGHT; y++) {
                tiles[x][y] = new Tile(TileType.AIR, x, y, 32, 32);
            }
        }
    }

    /* Load the map. This is called in the main class upon game start */
    public void load(File loadFile) {
        try {
            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(loadFile);
            Element root = document.getRootElement();

            for (Element tile : root.getChildren()) {
                int x = (int) Double.parseDouble(tile.getAttributeValue("x"));
                int y = (int) Double.parseDouble(tile.getAttributeValue("y"));
                tiles[x][y] = new Tile(TileType.valueOf(tile.getAttributeValue("type")), x, y, 32, 32);
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(TileGrid.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Loaded map: " + loadFile.getName());
    }

    public void save(File saveFile) {
        Document document = new Document();
        Element root = new Element("tiles");
        document.setRootElement(root);

        for (int x = 0; x < AMOUNT_TILES_WIDTH; x++) {
            for (int y = 0; y < AMOUNT_TILES_HEIGHT; y++) {
                Element tile = new Element("tile");
                System.out.println("X: " + tiles[x][y].getX() + " Y: " + tiles[x][y].getY() + " Type: " + String.valueOf(tiles[x][y].getType()));
                tile.setAttribute("x", String.valueOf(tiles[x][y].getX()));
                tile.setAttribute("y", String.valueOf(tiles[x][y].getY()));
                tile.setAttribute("type", String.valueOf(tiles[x][y].getType()));
                root.addContent(tile);
            }
        }

        XMLOutputter output = new XMLOutputter();
        try {
            output.output(document, new FileOutputStream(new File("res/maps/map.xml")));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TileGrid.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TileGrid.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setAt(TileType type, int x, int y) {
        tiles[x][y] = new Tile(type, x, y, 32, 32);
    }

    public TileType getAt(int x, int y) {
        return tiles[x][y].getType();
    }

    public void update(double delta) {
        for (int x = 0; x < AMOUNT_TILES_WIDTH; x++) {
            for (int y = 0; y < AMOUNT_TILES_HEIGHT; y++) {
                tiles[x][y].update(delta);
            }
        }
    }

    public void render() {
        for (int x = 0; x < AMOUNT_TILES_WIDTH; x++) {
            for (int y = 0; y < AMOUNT_TILES_HEIGHT; y++) {
                tiles[x][y].render();
            }
        }
    }

    public static TileObject[][] getTiles() {
        return tiles;
    }

    public TileObject getObjectAt(int x, int y) {
        return tiles[x][y];
    }
}
