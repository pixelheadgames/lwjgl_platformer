package com.jakehorsfield.platformer.objects.tiles;

public enum TileType {
    NORMAL_TILE("res/tiles/default_tile.png", true),
    AIR("res/tiles/air_tile.png", false),
    GRASS_TILE("res/tiles/grass_tile.png", true),
    DIRT_TILE("res/tiles/dirt_tile.png", true),
    COIN("res/pickups/coin_pickup.png", false);

    public String texturePath;
    public boolean solidTile;

    private TileType(String texturePath, boolean solidTile) {
        this.texturePath = texturePath;
        this.solidTile = solidTile;
    }
}
