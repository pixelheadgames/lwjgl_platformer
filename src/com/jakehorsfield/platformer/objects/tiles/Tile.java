package com.jakehorsfield.platformer.objects.tiles;

import com.jakehorsfield.platformer.graphics.Textures;
import com.jakehorsfield.platformer.objects.TileObject;
import org.lwjgl.util.vector.Vector2f;

public class Tile extends TileObject {

    private boolean solidTile = false;

    public Tile(TileType type, float x, float y, float width, float height) {
        init(new Vector2f(x, y), width, height);
        this.texture = Textures.loadTexture(type.texturePath);
        this.type = type;
    }

    public void render() {
        Textures.drawTexture(texture, new Vector2f(position.x * 32, position.y * 32));
    }

    public void update(double delta) {
        if (type == TileType.COIN) {

        }
    }
}
