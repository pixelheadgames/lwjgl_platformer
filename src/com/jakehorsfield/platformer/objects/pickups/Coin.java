package com.jakehorsfield.platformer.objects.pickups;

import com.jakehorsfield.platformer.objects.PickupObject;
import com.jakehorsfield.platformer.player.Player;

public class Coin extends PickupObject {

    public Coin(float x, float y, String texture, Player player) {
        this.player = player;
    }

    public void onPickup() {
        System.out.println("Picked up");
    }

    public void update() {

    }

    public void render() {

    }

    public void update(double delta) {
        if (player.getBounds().intersects(this.getBounds())) {
            onPickup();
        }
    }
}
