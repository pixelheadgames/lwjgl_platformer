package com.jakehorsfield.platformer.objects;

import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public abstract class GameObject {

    protected Vector2f position;
    protected float width;
    protected float height;

    public abstract void render();

    public void init(Vector2f position, float width, float height) {
        this.position = position;
        this.width = width;
        this.height = height;
    }

    public abstract void update(double delta);

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public Rectangle getBounds() {
        return new Rectangle((int) getX(), (int) getY(), (int) getWidth(), (int) getHeight());
    }
}
