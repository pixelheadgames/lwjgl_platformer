package com.jakehorsfield.platformer.player;

import com.jakehorsfield.platformer.graphics.Textures;
import com.jakehorsfield.platformer.objects.MoveableObject;
import com.jakehorsfield.platformer.physics.Physics;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

import java.awt.*;

public class Player extends MoveableObject {

    private Texture texture;
    private Texture leftTexture;
    private Texture rightTexture;
    private double delta;
    private boolean collidingLeft;
    private boolean collidingRight;
    private boolean collidingTop;
    private boolean collidingBottom;

    public Player(Vector2f position, float width, float height) {
        init(position, width, height);
        texture = Textures.loadTexture("res/sprites/right_player.png");
        leftTexture = Textures.loadTexture("res/sprites/left_player.png");
        rightTexture = Textures.loadTexture("res/sprites/right_player.png");
    }

    public void getInput() {
        if (Keyboard.isKeyDown(Keyboard.KEY_UP) && movement.y < Physics.MAX_PLAYER_SPEED && !collidingBottom) {
            movement.y += Physics.ACCELERATION;
        } else if (Keyboard.isKeyDown(Keyboard.KEY_DOWN) && movement.y > -Physics.MAX_PLAYER_SPEED && !collidingTop) {
            movement.y -= Physics.ACCELERATION;
        } else if (Keyboard.isKeyDown(Keyboard.KEY_LEFT) && movement.x > -Physics.MAX_PLAYER_SPEED && !collidingRight) {
            movement.x -= Physics.ACCELERATION;
            texture = leftTexture;
        } else if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT) && movement.x < Physics.MAX_PLAYER_SPEED && !collidingLeft) {
            movement.x += Physics.ACCELERATION;
            texture = rightTexture;
        } else {
            movement.x = 0;
            movement.y = 0;
        }
    }

    public void update(double delta) {
        this.delta = delta;

        position.x += movement.x * delta;
        position.y += movement.y * delta;

        if (position.x <= 0) {
            position.x = 0;
            movement.x = 0;
        }
        if (position.x >= Display.getWidth() - width) {
            position.x = Display.getWidth() - width;
            movement.x = 0;
        }

        if (position.y <= 0) {
            position.y = 0;
            movement.y = 0;
        }
        if (position.y >= Display.getHeight() - height) {
            position.y = Display.getHeight() - height;
            movement.y = 0;
        }

        collidingLeft = Physics.hitsLeftOfTile(this);
        collidingRight = Physics.hitsRightOfTile(this);
        collidingTop = Physics.hitsTopOfTile(this);
        collidingBottom = Physics.hitsBottomOfTile(this);

    }

    public void render() {
        Textures.drawTexture(texture, position);
    }

    public void setPositionX(float x) {
        position.x = x;
    }

    public void setPositionY(float y) {
        position.y = y;
    }

    public Rectangle getBounds() {
        return new Rectangle((int) position.x, (int) position.y, (int) width, (int) height);
    }

    public void addToX(float amountToAdd) {
        position.x += amountToAdd;
    }

    public void addToY(float amountToAdd) {
        position.y += amountToAdd;
    }
}
