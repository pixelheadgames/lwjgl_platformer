package com.jakehorsfield.platformer.graphics;

import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL11.*;

public class Textures {

    public static void drawTexture(Texture texture, Vector2f position) {
        glEnable(GL_TEXTURE_2D);

        glPushMatrix();

        glColor3f(1.f, 1.f, 1.f);

        texture.bind();

        glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2f(position.x, position.y);

        glTexCoord2f(0, 1);
        glVertex2f(position.x, position.y + texture.getImageHeight());

        glTexCoord2f(1, 1);
        glVertex2f(position.x + texture.getImageWidth(), position.y + texture.getImageHeight());

        glTexCoord2f(1, 0);
        glVertex2f(position.x + texture.getImageWidth(), position.y);
        glEnd();

        glPopMatrix();

        glDisable(GL_TEXTURE_2D);
    }

    public static Texture loadTexture(String filename) {
        Texture t = null;
        try {
            t = TextureLoader.getTexture("png", new FileInputStream(new File(filename)));
        } catch (IOException ex) {
            Logger.getLogger(Textures.class.getName()).log(Level.SEVERE, "Error loading texture: " + filename, ex);
        }

        return t;
    }
}
