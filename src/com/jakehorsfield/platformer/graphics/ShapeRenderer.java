package com.jakehorsfield.platformer.graphics;

import org.lwjgl.util.vector.Vector2f;

import static org.lwjgl.opengl.GL11.*;

public class ShapeRenderer {

    public static void quad(float x, float y, float width, float height, float r, float g, float b) {
        glPushMatrix();

        glColor3f(r, g, b);

        glBegin(GL_QUADS);
        glVertex2f(x, y);
        glVertex2f(x, y + height);
        glVertex2f(x + width, y + height);
        glVertex2f(x + width, y);
        glEnd();

        glPopMatrix();
    }

    public static void quad(Vector2f position, float width, float height, float r, float g, float b, boolean filled) {
        glPushMatrix();

        glColor3f(r, g, b);

        if (filled) {
            glBegin(GL_QUADS);
            glVertex2f(position.x, position.y);
            glVertex2f(position.x, position.y + height);
            glVertex2f(position.x + width, position.y + height);
            glVertex2f(position.x + width, position.y);
            glEnd();
        } else {
            glBegin(GL_LINES);
            glVertex2f(position.x, position.y);
            glVertex2f(position.x, position.y + height);

            glVertex2f(position.x, position.y + height);
            glVertex2f(position.x + width, position.y + height);

            glVertex2f(position.x + width, position.y + height);
            glVertex2f(position.x + width, position.y);

            glVertex2f(position.x + width, position.y);
            glVertex2f(position.x, position.y);
            glEnd();
        }

        glPopMatrix();
    }

    public static void triangle(float x, float y, float width, float height, float r, float g, float b) {
        glPushMatrix();

        glColor3f(r, g, b);

        glBegin(GL_TRIANGLES);
        glVertex2f(x, y);
        glVertex2f(x + (width / 2), y + height);
        glVertex2f(x + width, y);
        glEnd();

        glPopMatrix();
    }
}
