package com.jakehorsfield.platformer.base;

import com.jakehorsfield.platformer.graphics.ShapeRenderer;
import com.jakehorsfield.platformer.graphics.Text;
import com.jakehorsfield.platformer.player.Player;
import org.lwjgl.opengl.Display;

import java.text.DecimalFormat;

public class Debug {

    public static boolean debugMode = true;
    private static DecimalFormat formatter = new DecimalFormat("#.##");

    public static void printDebug(Player player) {
        ShapeRenderer.quad(10, Display.getHeight() - 300, 100, 300, 0.f, 0.f, 0.f);
        Text.drawString("px: " + formatter.format(player.getX()) + " py: " + formatter.format(player.getY()) +
                "\n\nvx: " + formatter.format(player.getMovementX()) + " vy: " + formatter.format(player.getMovementY()), 10, Display.getHeight() - 20, 1.f, 1.f, 1.f);
    }
}
