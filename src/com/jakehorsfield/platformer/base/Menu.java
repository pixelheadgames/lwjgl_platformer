package com.jakehorsfield.platformer.base;

import com.jakehorsfield.platformer.graphics.Textures;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

public class Menu {

    private Texture playButton;
    private Texture aboutButton;
    private Texture exitButton;

    public Menu() {
        playButton = Textures.loadTexture("res/menu/play.png");
        aboutButton = Textures.loadTexture("res/menu/about.png");
        exitButton = Textures.loadTexture("res/menu/exit.png");
    }

    public void render() {
        Textures.drawTexture(playButton, new Vector2f(Display.getWidth() / 2 - playButton.getImageWidth() / 2, 300));
        Textures.drawTexture(aboutButton, new Vector2f(Display.getWidth() / 2 - playButton.getImageWidth() / 2, 150));
        Textures.drawTexture(exitButton, new Vector2f(Display.getWidth() / 2 - playButton.getImageWidth() / 2, 0));
    }

    public void getInput() {
        // TODO: Change state based on mouse input
    }
}
