package com.jakehorsfield.platformer.states;

public enum State {
    MENU,
    ABOUT,
    PLAY;

    private static State currentState = MENU;

    public static State getState() {
        return currentState;
    }

    public static void setState(State state) {
        currentState = state;
    }
}
