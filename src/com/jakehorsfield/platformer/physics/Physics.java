package com.jakehorsfield.platformer.physics;

import com.jakehorsfield.platformer.objects.TileObject;
import com.jakehorsfield.platformer.objects.tiles.TileGrid;
import com.jakehorsfield.platformer.player.Player;
import org.lwjgl.util.Rectangle;

public class Physics {

    public static float MAX_PLAYER_SPEED = 15.f;
    public static final float GRAVITY = -2.f;
    public static final float ACCELERATION = 0.5f;
    public static final float FRICTION = 0.9f;
    public static final float JUMP_FRICTION = 0.5f;

    private static Rectangle tileRectangle = new Rectangle();
    private static Rectangle playerRectangle = new Rectangle();

    public static boolean hitsLeftOfTile(Player player) {
        TileObject[][] tiles = TileGrid.getTiles();

        playerRectangle.setBounds((int) player.getX(), (int) player.getY(), (int) player.getWidth(), (int) player.getHeight());

        for (int x = 0; x < TileGrid.AMOUNT_TILES_WIDTH; x++) {
            for (int y = 0; y < TileGrid.AMOUNT_TILES_HEIGHT; y++) {
                tileRectangle.setBounds(((int) tiles[x][y].getX() * 32) - 1, (int) tiles[x][y].getY() * 32, 1, (int) tiles[x][y].getHeight());

                if (tileRectangle.intersects(playerRectangle) && tiles[x][y].getType().solidTile) {
                    MAX_PLAYER_SPEED = 0.f;
                    return true;
                }
            }
        }

        MAX_PLAYER_SPEED = 15.f;
        return false;
    }

    public static boolean hitsRightOfTile(Player player) {
        playerRectangle.setBounds((int) player.getX(), (int) player.getY(), (int) player.getWidth(), (int) player.getHeight());

        TileObject[][] tiles = TileGrid.getTiles();

        for (int x = 0; x < TileGrid.AMOUNT_TILES_WIDTH; x++) {
            for (int y = 0; y < TileGrid.AMOUNT_TILES_HEIGHT; y++) {
                tileRectangle.setBounds(((int) tiles[x][y].getX() * 32) + (int) tiles[x][y].getWidth(), (int) tiles[x][y].getY() * 32, 1, (int) tiles[x][y].getHeight());

                if (tileRectangle.intersects(playerRectangle) && tiles[x][y].getType().solidTile) {
                    MAX_PLAYER_SPEED = 0.f;
                    return true;
                }
            }
        }

        MAX_PLAYER_SPEED = 15.f;
        return false;
    }

    public static boolean hitsTopOfTile(Player player) {
        playerRectangle.setBounds((int) player.getX(), (int) player.getY(), (int) player.getWidth(), (int) player.getHeight());

        TileObject[][] tiles = TileGrid.getTiles();

        for (int x = 0; x < TileGrid.AMOUNT_TILES_WIDTH; x++) {
            for (int y = 0; y < TileGrid.AMOUNT_TILES_HEIGHT; y++) {
                tileRectangle.setBounds(((int) tiles[x][y].getX() * 32), ((int) tiles[x][y].getY() * 32) + (int) tiles[x][y].getHeight(), (int) tiles[x][y].getWidth(), 1);

                if (tileRectangle.intersects(playerRectangle) && tiles[x][y].getType().solidTile) {
                    MAX_PLAYER_SPEED = 0.f;
                    return true;
                }
            }
        }

        MAX_PLAYER_SPEED = 15.f;
        return false;
    }

    public static boolean hitsBottomOfTile(Player player) {
        playerRectangle.setBounds((int) player.getX(), (int) player.getY(), (int) player.getWidth(), (int) player.getHeight());

        TileObject[][] tiles = TileGrid.getTiles();

        for (int x = 0; x < TileGrid.AMOUNT_TILES_WIDTH; x++) {
            for (int y = 0; y < TileGrid.AMOUNT_TILES_HEIGHT; y++) {
                tileRectangle.setBounds(((int) tiles[x][y].getX() * 32), ((int) tiles[x][y].getY() * 32) - 1, (int) tiles[x][y].getWidth(), 1);

                if (tileRectangle.intersects(playerRectangle) && tiles[x][y].getType().solidTile) {
                    MAX_PLAYER_SPEED = 0.f;
                    return true;
                }
            }
        }

        MAX_PLAYER_SPEED = 15.f;
        return false;
    }
}
